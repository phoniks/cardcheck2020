import React from 'react'
import Link from 'next/link'
import {UserConsumer, UserContext} from './Components/UserContext';
import { Box, Text, TextInput, Label, Checkbox, Button, Window } from 'react-desktop/macOs';
import translateErrors from '../lib/translateErrors'

export default class AuthForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      firstName: '',
      lastName: '',
      email: '',
      handle: '',
      network: '',
      phone: '',
      address: '',
      city: '',
      zipcode: '',
      employer: '',
      location: '',
      title: '',
      organizer: false
    };

  }

  handleChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
    alert('A name was submitted: ' + this.state.firstName);
    event.preventDefault();
  }


  render() {
    this.state = this.context
    this.handleChange = this.context.handleChange
    this.handleSubmit = this.context.handleSubmit
    return (
      <Box>
        { this.state.errorMessage && <h3 className="error"> { translateErrors(this.state.errorMessage) } </h3> }
      <div>
        <Box> <p>I hereby suport the creation of a union of employees for the purposes of collective bargaining at my workplace. Upon the creation of such a union, I authorize it to bargain with my employer on my behalf.</p> </Box>
        <br></br>
        <form onSubmit={this.handleSubmit}>
          <Label>
            First Name:
            <TextInput type="text" name='firstName' value={this.state.firstName} onChange={e => this.context.handleChange(e)} />
          </Label>
          <br></br>
          <Label>
            Last Name:
            <TextInput type="text" name='lastName' value={this.state.lastName} onChange={e => this.handleChange(e)} />
          </Label>
          <br></br>
          <Label>
            Email Address:
            <TextInput type="text" name='email' value={this.state.email} onChange={e => this.handleChange(e)} />
          </Label>
          <br></br>
          <Label>
            Social Media Handle or URL (optional):
            <TextInput type="text" name='handle' value={this.state.handle} onChange={e => this.handleChange(e)} />
          </Label>
          <br></br>
          Social media Network:
            <select name='network' value={this.state.network}  onChange={e => this.handleChange(e)}>
              <option value="facebook">Facebook</option>
              <option value="twitter">Twitter</option>
              <option value="instagram">Instagram</option>
              <option value="linkedin">LinkedIn</option>
            </select>
            <br></br>
          <Label> 
            Phone Number:
            <TextInput type="text" name='phone' value={this.state.phone} onChange={e => this.handleChange(e)} />
          </Label>
          <br></br>
          <Label> 
            Home Address:
            <TextInput type="text" name='address' value={this.state.address} onChange={e => this.handleChange(e)} />
          </Label>
          <br></br>
          <Label> 
            City:
            <TextInput type="text" name='city' value={this.state.city} onChange={e => this.handleChange(e)} />
          </Label>
          <br></br>
          <Label> 
            Zipcode:
            <TextInput type="text" name='zipcode' value={this.state.zipcode} onChange={e => this.handleChange(e)} />
          </Label>
          <br></br>
          <Label> 
            Employer:
            <TextInput type="text" name='employer' value={this.state.employer} onChange={e => this.handleChange(e)} />
          </Label>
          <br></br>
          <Label> 
            Work Location:
            <TextInput type="text" name='location' value={this.state.location} onChange={e => this.handleChange(e)} />
          </Label>
          <br></br>
          <Label> 
            Title:
            <TextInput type="text" name='title' value={this.state.title} onChange={e => this.handleChange(e)} />
          </Label>
            <br></br>
            <Label>
              I'd like to participate in the Union Organizing Committee:
            <Checkbox type="checkbox" name='organizer' onChange={e => this.handleChange(e)}/>
            </Label>
            <br></br>
            <br></br>
           <Link href='Confirm'>
           <input size='24' type="submit" value="Submit" onSubmit={e => this.handleSubmit(e)}/>
           </Link>      
        </form>
      </div>
     </Box> 
    );
  }
}

AuthForm.contextType = UserContext
