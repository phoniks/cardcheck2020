import {Component} from 'react'
import Link from 'next/link'
import {UserContext} from './Components/UserContext'
import { Box, Text, TextInput, Label, Checkbox, Button, Window } from 'react-desktop/macOs';
const languageChoices = [['EN', 'English'], ['SP', 'Spanish'], ['CH','Chinese']]
const makeOptions = function(X) {
  return <option value={X[0]}>{X[1]}</option>;
};
const languageSelector = (languageChoices, handlerFunction, state) => {
  return (<select name='language' value={state.language}  onChange={e => handlerFunction(e)}>
            {languageChoices.map((languageOption) => makeOptions(languageOption))}
          </select>
)}

export default class Introduction extends Component {

  render(){
    switch (this.context.language ){
      case 'SP':
      return  (<Box>
        {languageSelector(languageChoices, this.context.changeLanguage, this.context)}
        <br/><br/>
        <Text>En los Estados Unidos, los trabajadores tienen derecho a organizarse con el fin de abogar por mejores salarios y mejores condiciones laborales.
    Este derecho está protegido por leyes que prohíben al empleador de castigar, intimidar, coaccionar o despedir a los empleados que ejercen dicho derecho. Este formulario está diseñado para permitirle expresar su interés en formar una Unión con otros empleados en su lugar de trabajo.
<b>El firmar este formulario de autorización es algo privado y nunca se revelará a su empleador.</b></Text><br/>
<Text>Al firmar este formulario, usted esta autorizando la formación de un sindicato para representar sus intereses y los de otros empleados en la mesa de negociaciones con su empleador.
    Si el 50% de los empleados en su lugar de trabajo firman este formulario, la Junta Nacional de Relaciones Laborales exigirá a su empleador que reconozca a su sindicato.
    Si el 30% de los empleados en su lugar de trabajo firman este formulario, puede solicitar a la NLRB una elección secreta para determinar si el 50% de los empleados están a favor de formar o afiliarse a un sindicato
    Si menos del 30% de los empleados en su lugar de trabajo firman este formulario, no pasará nada. Usted NO PUEDE ser despedido o discriminado legalmente por firmar.<br/><br/>
<b>Un empleador quien lo despida por firmar esto, o incluso solo por hablar de organizarse, se verá obligado a restablecer su trabajo, Y le pagará por el tiempo que no se le permitió trabajar.</b></Text>
        <br/><br/>
        <Link href="/Authorize">
          <a>Continuar</a>
        </Link>
      </Box>);
      case 'EN':
        return (<Box>
          {languageSelector(languageChoices, this.context.changeLanguage, this.context)}
          <br/><br/>
          <Text>In the United States workers have a right to organize themselves for the purpose of advocating for better pay and working conditions.
          This right is protected by laws prohibiting the employer from punishing, intimidating, coercing, or firing employees who exercise it.
          This form is designed to allow you to express your interest in forming a Union with other employees at your workplace.
          <b>Signing this Authorization Form is private and will never be revealed to your employer.</b> </Text><br/>
          <Text>By signing this form you are Authorizing the formation of a Union to represent your and other employee's interests at the bargaining table with your employer.
          If 50% of employees at your workplace sign this form, your employer will be required by the National Labor Relations Board to recognize your union.
          If 30% of employees at your workplace sign this form, you can petition the NLRB for a secret ballot election to determine if 50% of employees are in favor of forming or joining a union
          If less than 30% of employees at your worplace sign this form, nothing will happen. You CAN NOT legally be fired or discriminated against for signing.  <br/><br/>
          <b> An employer who fires you for signing this, or even just for talking about organizing will be forced to reinstate your job, AND pay you for the time you weren't allowed to work.</b></Text>
          <br/><br/>
          <Link href="/Authorize">
            <a>Continue</a>
          </Link>
        </Box>);
      case 'CH':  
      return (<Box>
          {languageSelector(languageChoices, this.context.changeLanguage, this.context)}
          <br/><br/>
          在美国，工人有权组织自己，以提倡更好的工资和工作条件。
          这项权利受到法律的保护，禁止雇主惩罚，恐吓，胁迫或解雇行使该权利的雇员。
          该表格旨在使您表达出与工作场所中其他员工组成工会的兴趣。
          签署此授权表是私人的，绝不会透露给您的雇主。
          签署此表即表示您同意成立一个工会，与您的雇主在谈判桌上代表您和其他雇员的利益。
          如果您的工作场所中有50％的员工在此表上签名，则国家劳动关系委员会将要求您的雇主承认您的工会。
          如果您工作场所中30％的员工签署此表，则您可以向NLRB申请无记名投票，以确定50％的员工是否赞成成立或加入工会。
          如果您工作场所中不到30％的员工签署此表格，则不会发生任何事情。不能合法解雇或歧视您进行签名。
           雇主因签署或解雇甚至解雇您而被解雇，这将被迫恢复您的工作，并为您不允许工作的时间付钱。
          <br/> <br/>
          <Link href="/authorize">
            <a>Continue</a>
          </Link>
          </Box>)
          
    }
    }
} 

Introduction.contextType = UserContext