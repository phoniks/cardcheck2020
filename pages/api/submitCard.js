'use strict';
import insertEmployee from './insertEmployee.js';
import sendConfirmation from './sendConfirmation.js';

//function which takes employeeData {} 
//inserts a new document into the db
//if successful send confirmation email
//if fail return error

export default async (req, res) => {
  await insertEmployee(req.body, (error, result) => {
    if(error){
      console.log('emp@submitCard: ')
      console.log(req.body)
      console.error(error)
      if(error.name == 'ValidationError'){
        res.status('400').json(error)
      } else{
        res.status('500').json(error)
      } 
    } else {
      console.log(result)
      sendConfirmation(req.body, (error, result) => {
        if(error){
          console.log(error)
          res.status('503').json(error)
        } else { res.status('200').json(result)}
      })
    }
  })
};