const dynamoose = require('dynamoose')
require('dotenv').config()
dynamoose.AWS.config.region = 'us-east-1'
dynamoose.AWS.config.update({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: process.env.AWS_REGION,
    sessionToken: process.env.AWS_SESSION_TOKEN
})
const EmployeeSchema = require('./Models/Employee.js')
const EmployeeModel = dynamoose.model('Employee', EmployeeSchema, {waitForActiveTimeout: 5000})

export default async (employee, callback) => {

    console.log('employee@insert: ')
    console.log(employee)
  
    const Employee = new EmployeeModel({
        firstName: employee.firstName,
        lastName: employee.lastName,
        email: employee.email,
        url: employee.handle,
        network: employee.network,
        phone: employee.phone,
        address: employee.address,
        city: employee.city,
        zipcode: employee.zipcode,
        employer: employee.employer,
        location: employee.location,
        title: employee.title, 
        organizer: employee.organizer, 
    });

    await Employee.save(callback)
  }