const dynamoose = require('dynamoose');
const validatorLib = require('validator');
const socialMediaNetworkOptions = [
    'Facebook',
    'Twitter',
    'Instagram',
    'LinkedIn'
]
const employee = new dynamoose.Schema( {
  lastName: {
    type: String,
    required: true,
    validate:  (lastName) => validatorLib.isAlphanumeric(lastName)
  },
  firstName: {
    type: String,
    require: true,
    validate: (firstName) => validatorLib.isAlphanumeric(firstName)
  },
  socialMediaURL: {
    type: String,
    validate: (socialMediaURL) => validatorLib.isURL(socialMediaURL)
  },
  socialMediaNetwork: {
    type: String,
    validate: (socialMediaNetwork) => validatorLib.isIn(socialMediaNetwork,socialMediaNetworkOptions)
  },
  email: {
    type: String,
    required: true,
    validate: (email) => validatorLib.isEmail(email)
  },
  phone: {
    type: String,
    required: true,
    validate: (phone) => validatorLib.isMobilePhone(phone, ['en-US'])
  },
  address: {
    type: String,
    required: true,
    validate: (address) => {
      return address.split(' ').every(function (str) { return validatorLib.isAlphanumeric(str)})
    } 
  },
  city: {
    type: String,
    required: true,
    validate: city => { return city.split(' ').every(function (str) {return validatorLib.isAlphanumeric(str)})}
  },
  zipcode: {
    type: String,
    required: true,
    validate: (zipcode) => validatorLib.isPostalCode(zipcode,'US')
  },
  employer: {
    type: String,
    required: true,
    validate: (employer) => validatorLib.equals(employer, 'Amazon.com Services Inc.')
  },
  location: {
    type: String,
    required: true,
    validate: location => validatorLib.equals(location, 'DSF4')
  },
  title: {
    type: String,
    required: true,
    validate: title => { return title.split(' ').every(function (str) {return validatorLib.isAlphanumeric(str)})
  }
  },
  organizer: {
    type: Boolean,
    required: true,
  },
});

module.exports = employee;

