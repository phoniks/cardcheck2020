import React from 'react';
import App from 'next/app';
import UserProvider from './Components/UserContext'

export default class AuthCard extends App {
    render() {
        const { Component, pageProps } = this.props;

        return (
                <UserProvider>
                    <Component {...pageProps} />
                </UserProvider>
        );
    }
};