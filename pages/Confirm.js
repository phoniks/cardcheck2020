import { useContext, useState, Component } from 'react';
import {UserConsumer} from './Components/UserContext';
import Link from 'next/link'

export default class Confirm extends Component{
  render(){
    return (
      <UserConsumer>
        {({firstName,lastName,email,handle,network,phone,address, city, zipcode, employer, location , title, organizer, handleSubmit}) =>
          (<div>
            <p>I hereby advocate for the creation of a Union of employees for the purposes of collective bargaining at my workplace. Upon the creation of such a Union, I authorize it to bargain with my employer on my behalf.</p>
              <div>
                First Name: {firstName} <br></br>
                Last Name: {lastName} <br></br>
                Email Address: {email} <br></br>
                Social Media Handle or URL: {handle} <br></br>
                Social media Network: {network} <br></br>
                Phone Number: {phone} <br></br>
                Home Address: {address} <br></br>
                City: {city} <br></br>
                Zipcode: {zipcode} <br></br>
                Employer: {employer} <br></br>
                Work Location: {location} <br></br>
                Title: {title} <br></br>
                I'd like to participate in the Union Organizing Committee:  {organizer ? "Yes" : "No"}
                <br></br>
                <br></br>
              <Link href='Confirm'>
                <input type="submit" value="Submit" onClick={handleSubmit} />
              </Link> 
              </div> 
          </div> )
        } 
      </UserConsumer> 
    );
  }     
}
