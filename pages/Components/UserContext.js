import React, { Component } from 'react'
import Router from 'next/router'


const UserContext = React.createContext()

class UserProvider extends Component {
  state = {
    language: 'EN',
    firstName: '',
    lastName: '',
    email: '',
    handle: '',
    network: 'Facebook',
    phone: '',
    address: '',
    city: '',
    zipcode: '',
    employer: 'Amazon.com Services Inc.',
    location: 'DSF4',
    title: 'Sortation Associate',
    organizer: false,
  }

  handleChange (e) {
      e.preventDefault()
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({ [name]: value });
  }

  changeLanguage (e){ 
    e.preventDefault()
    const target = e.target
    const value = target.value
    const name = target.name
      this.setState({[name]: value})
  }

  raiseError (error){
    this.setState({errorMessage: error.message})
  }

  async handleSubmit(){
    try {
      console.log('Employee at handleSubmit: '+this.state)
      const response = await fetch('api/submitCard', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(this.state)
      })
      if (response.ok) {
        Router.push('/thanks')
      }
      if (response.status == '400'){
        response.json().then((validationError) =>{
          this.raiseError(validationError)
          Router.push('/authorize')
        })
        
      }
    } catch (error) {
      console.error(
        'You have an error in your code or there are Network issues.',
        error
      )
      throw new Error(error)
      }
    }  

  render () {
    return (
      <UserContext.Provider
        value={{
            errorMessage: this.state.errorMessage,
            language: this.state.language,
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            email: this.state.email,
            handle: this.state.handle,
            network: this.state.network,
            phone: this.state.phone,
            address: this.state.address,
            city: this.state.city,
            zipcode: this.state.zipcode,
            employer: this.state.employer,
            location: this.state.location,
            title: this.state.title,
            organizer: this.state.organizer,
            handleChange: this.handleChange.bind(this),
            changeLanguage: this.changeLanguage.bind(this),
            handleSubmit: this.handleSubmit.bind(this)
        }}
      >
        {this.props.children}
      </UserContext.Provider>
    )
  }
}

const UserConsumer = UserContext.Consumer

export default UserProvider
export { UserConsumer, UserContext }