import {Component} from 'react'
import Link from 'next/link'
const socialMediaNetworkOptions = [
    ['Facebook', 'FB' ],
    ['Twitter', 'TW' ],
    ['Instagram','IG' ],
    ['LinkedIn', 'LI' ]
]

export default class AuthForm extends Component {
    constructor (props){
        super(props);
        this.state = {
            firstName: props.firstName,
            lastName: props.lastName,
            email: props.email,
            url: props.url,
            network: props.network,
            phone: props.phone,
            address: props.address,
            employer: props.employer,
            location: props.location,
            title: props.title, 
            organizing: props.organizing, 
        }
       this.handleChange = props.handleChange
    }

    render (){
        const makeOptions = function(X) {
            return <option>{X}</option>;
        };

        return (
            <form onSubmit={this.handleChange}>
                <label>
                First Name:
                <input type="text" name='firstName' value={this.state.firstName} onChange={e => this.handleChange(e)} />
                </label>
                <br></br>

                <label>
                Last Name:
                <input type="text" name='lastName' value={this.state.lastName} onChange={e => this.handleChange(e)} />
                </label>
                <br></br>

                <label>
                Email Address:
                <input type="text" name='email' value={this.state.email} onChange={e => this.handleChange(e)} />
                </label>
                <br></br>

                <label>
                Social Media URL (optional):
                <input type="text" name='url' value={this.state.url} onChange={e => this.handleChange(e)} />
                </label>

                <br></br>
                <label>
                Social media Network:
                <select name='network' value={this.state.network}  onChange={e => this.handleChange(e)}>
                    {makeOptions(socialMediaNetworkOptions)}
                </select>
                </label>
                <br></br>

                <label> 
                Phone Number:
                <input type="text" name='phone' value={this.state.phone} onChange={e => this.handleChange(e)} />
                </label>
                <br></br>

                <label> 
                Home Address:
                <input type="text" name='address' value={this.state.address} onChange={e => this.handleChange(e)} />
                </label>
                <br></br>
                
                <label> 
                City:
                <input type="text" name='city' value={this.state.city} onChange={e => this.handleChange(e)} />
                </label>
                <br></br>

                <label> 
                Zip Code:
                <input type="text" name='zipcode' value={this.state.zipcode} onChange={e => this.handleChange(e)} />
                </label>
                <br></br>

                <label> 
                Employer:
                <input type="text" name='employer' value={this.state.employer} onChange={e => this.handleChange(e)} />
                </label>
                <br></br>

                <label> 
                Work Location:
                <input type="text" name='location' value={this.state.location} onChange={e => this.handleChange(e)} />
                </label>
                <br></br>

                <label> 
                Title:
                <input type="text" name='title' value={this.state.title} onChange={e => this.handleChange(e)} />
                </label>
                <br></br>

                <label>
                    I'd like to participate in the Union Organizing Committee:
                <input type="checkbox" name='organizing' value={this.state.organizing} onChange={e => this.handleChange(e)}/>
                </label>
                <br></br>
                <br></br>
            </form>
        )
    }
}